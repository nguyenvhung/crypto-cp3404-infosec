import java.util.regex.*;
import java.util.*;

public class crypto {

    public static void main(String[] args) {

        String crypto = "bqcu wcuhjlust, hctwrqecdwat jl rau xlu qz rcdklzqcbdrjqkl qz odrd jkrukouo rq bdvu rau odrd xlusull rq qwwqkukrl, nxr budkjkezxs rq suejrjbdru cuhujgucl. rau qkst luhcur wdcr qz dsbqlr dss bqouck hctwrqecdwajh ltlrubl, aqfuguc, jl rau vut --rau wdcdburuc radr lusuhrl rau wdcrjhxsdc rcdklzqcbdrjqk rq nu ubwsqtuo.";

        String lettres_regex = "[A-Za-z]";

        String decrypted_string = "";

        Hashtable<String, String> crypto_key_value = new Hashtable<String, String>();
        crypto_key_value.put("r", "t");
        crypto_key_value.put("a", "h");
        crypto_key_value.put("u", "e");
        crypto_key_value.put("d", "a");
        crypto_key_value.put("o", "d");
        crypto_key_value.put("q", "o");
        crypto_key_value.put("w", "p");
        crypto_key_value.put("e", "g");
        crypto_key_value.put("k", "n");
        crypto_key_value.put("l", "s");
        crypto_key_value.put("z", "f");
        crypto_key_value.put("j", "i");
        crypto_key_value.put("t", "y");
        crypto_key_value.put("b", "m");
        crypto_key_value.put("c", "r");
        crypto_key_value.put("h", "c");
        crypto_key_value.put("s", "l");
        crypto_key_value.put("x", "u");
        crypto_key_value.put("v", "k");
        crypto_key_value.put("n", "b");
        crypto_key_value.put("f", "w");
        crypto_key_value.put("g", "v");

        Collection<String> allkeys = crypto_key_value.keySet();

        for (int i = 0; i < crypto.length(); i++) {
            if (Pattern.matches(lettres_regex, Character.toString(crypto.charAt(i)))) {

                if (allkeys.contains(Character.toString(crypto.charAt(i)))) {
                    decrypted_string += crypto_key_value.get(Character.toString(crypto.charAt(i)));
                } else {
                    decrypted_string += '*';
                }
            } else {
                decrypted_string += crypto.charAt(i);
            }
        }

        System.out.println(decrypted_string);

    }
}